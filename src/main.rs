use std::env;

fn main() {
    // Get command line arguments
    let args: Vec<String> = env::args().collect();

    // Check if the correct number of arguments are provided
    if args.len() != 4 {
        eprintln!("Usage: {} <amount_invested> <months_invested> <yearly_interest>", args[0]);
        std::process::exit(1);
    }

    // Parse input parameters
    let amount_invested: f64 = args[1].parse().expect("Invalid amount invested");
    let months_invested: u32 = args[2].parse().expect("Invalid number of months");
    let yearly_interest: f64 = args[3].parse().expect("Invalid yearly interest");

    // Calculate future value using compound interest formula
    let monthly_interest_rate = 1.0 + yearly_interest / 100.0 / 12.0;
    let future_value = amount_invested * monthly_interest_rate.powi(months_invested as i32);

    // Display the future value
    println!("Future value after {} months: {:.2}", months_invested, future_value);
}
