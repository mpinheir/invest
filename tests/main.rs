use std::process::Command;

#[test]
fn test_investment_calculator() {
    // Test case 1
    let output = Command::new("cargo")
        .arg("run")
        .arg("--")
        .arg("1000")
        .arg("12")
        .arg("5")
        .output()
        .expect("Failed to run the command");

    let expected_output = "Future value after 12 months: 1051.16\n";
    assert_eq!(String::from_utf8_lossy(&output.stdout), expected_output);
    assert!(output.status.success());

    // Add more test cases as needed
}
