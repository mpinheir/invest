# Investment Calculator

This Rust program calculates the future value of an investment based on the provided parameters:

- Amount Invested
- Number of Months the Amount will Remain Invested
- Yearly Interest Rate

## Usage

1. **Clone the Repository:**

    ```bash
    git clone [https://gitlab.com/mpinheir/invest.git](https://gitlab.com/mpinheir/invest.git)
    ```

2. **Navigate to the Project Directory:**

    ```bash
    cd invest
    ```

3. **Run Functional Tests and Build the Final Binary:**

    ```bash
    make
    ```

    This command runs functional tests using `cargo test`, builds the final binary in release mode using `cargo build --release`, and installs the binary to the `~/bin` directory.

4. **Update the PATH:**

    Add the following line to your shell profile file. Depending on your shell, it could be `~/.bashrc` for Bash or `~/.zshrc` for Zsh:

    - For Bash (`~/.bashrc`):

        ```bash
        export PATH="$HOME/bin:$PATH"
        ```

    - For Zsh (`~/.zshrc`):

        ```bash
        export PATH="$HOME/bin:$PATH"
        ```

    After updating the profile file, restart your shell or run:

    ```bash
    source ~/.bashrc   # For Bash
    source ~/.zshrc    # For Zsh
    ```

5. **Run the Program:**

    ```bash
    invest <amount_invested> <months_invested> <yearly_interest>
    ```

    Replace `<amount_invested>`, `<months_invested>`, and `<yearly_interest>` with the actual values you want to use.

    Example:

    ```bash
    invest 1000 12 5
    ```

6. **Result:**

    The program will display the future value of the investment after the specified number of months based on the yearly interest rate.

## Experiment Details

This program was built by **Marcelo Pinheiro** as an experiment using ChatGPT version 3.5.

## Makefile Options

- **Run Functional Tests:**

    ```bash
    make test
    ```

- **Build the Final Binary:**

    ```bash
    make build
    ```

- **Install the Binary to ~/bin directory:**

    ```bash
    make install
    ```

- **Clean Up Build Artifacts:**

    ```bash
    make clean
    ```

- **Remove the Installed Binary from ~/bin:**

    ```bash
    make uninstall
    ```

These Makefile options provide various tasks for testing, building, installing, cleaning, and uninstalling. Use them based on your needs.

## License

This project is licensed under the **[MIT License](https://opensource.org/licenses/MIT)** - see the [LICENSE](LICENSE) file for details.
