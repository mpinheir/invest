# Makefile for Investment Calculator

# Default target
all: test build

# Run functional tests
test:
	cargo test

# Build the final binary
build:
	cargo build --release

# Install the binary to ~/bin directory
install:
	mkdir -p ~/bin
	cp target/release/invest ~/bin/

# Clean up build artifacts
clean:
	cargo clean

# Remove installed binary from ~/bin
uninstall:
	rm -f ~/bin/invest
